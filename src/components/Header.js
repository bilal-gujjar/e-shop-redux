import { useSelector } from "react-redux";
import Products from './Products'
import Button from "react-bootstrap/Button";
import { Link } from "react-router-dom";
const Header = () => {
  const count = useSelector((state) => state.cart.count);
  return (
    <nav>
      <div>
        <div>
          <h4>E-commerce Store with the help of React and Redux</h4>
          <Button variant="success"><Link to="/cart">Cart  : {count} </Link></Button>
        </div>
        <Products/>
      </div>
    </nav>
  );
}; 

export default Header;
