import { addToCart, countCart, totalCart } from "../shop/cart/actionCreators";
import { useDispatch } from "react-redux";
import Button from "react-bootstrap/Button";
import Card from "react-bootstrap/Card";


const Product = ({ product }) => {
  const dispatch = useDispatch();
  return (
    <div>
      <Card style={{ width: "18rem", height: "30rem" }}>
        <Card.Img
          height="200rem"
          variant="top"
          src={product.image}
          alt="prodc=uct"
        />
        <Card.Body>
          <Card.Title>{product.title}</Card.Title>
          <Card.Text>
            <p className="text-center">{product.category}</p>
            <b className="text-center">Rs :{product.price}</b>
          </Card.Text>

          <Button
            variant="primary"
            onClick={() => {
              dispatch(addToCart({ ...product, qty: 1, total: product.price }));
              dispatch(countCart());
              dispatch(totalCart());
            }}
          >
            Cart
          </Button>
        </Card.Body>
      </Card>
      <br />
    </div>
  );
};
export default Product;
