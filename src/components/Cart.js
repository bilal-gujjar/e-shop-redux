import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import Button from "react-bootstrap/Button";
import Card from "react-bootstrap/Card";
import {
  removeFromCart,
  countCart,
  totalCart,
  emptyCart,
  increaseQTY,
  decreaseQTY,
} from "../shop/cart/actionCreators";

const Cart = () => {
  const { products, total } = useSelector((state) => state.cart);
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(countCart());
    dispatch(totalCart());
  }, [dispatch]);
  return (
    <div>
      <div>
        <div>
          <div>
            {products.length > 0 &&
              products.map((product) => (
                <div key={product.id}>
                  <Card style={{ width: "18rem" }}>
                    <Card.Img
                      variant="top"
                      src={product.image}
                      alt={product.title}
                    />
                    <Card.Body>
                      {/* <Card.Title>{product.id}</Card.Title> */}
                      <Card.Title>{product.title}</Card.Title>

                      <div>
                        <Button
                          variant="info"
                          onClick={() => {
                            dispatch(decreaseQTY(product.id));
                            dispatch(countCart());
                            dispatch(totalCart());
                          }}
                        >
                          -
                        </Button>
                        {product.qty}
                        <Button
                          variant="info"
                          onClick={() => {
                            dispatch(increaseQTY(product.id));
                            dispatch(countCart());
                            dispatch(totalCart());
                          }}
                        >
                          +
                        </Button>
                      </div>
                      {/* </Card.Body> */}
                      <div>
                        <b>Unit Price Rs :{product.price}</b>

                        <b>Unit Total${parseFloat(product.total).toFixed(2)}</b>
                        <b>
                          Unit Tax:{parseFloat(product.total * 0.05).toFixed(2)}
                        </b>
                      </div>
                      <div>
                        <button
                          className="btn btn-danger"
                          onClick={() => {
                            dispatch(removeFromCart(product.id));
                            dispatch(countCart());
                            dispatch(totalCart());
                          }}
                        >
                          Delete
                        </button>
                      </div>
                    </Card.Body>
                  </Card>
                  <br />
                </div>
              ))}

            {products.length > 0 ? (
              <Card border="primary" className="offset-9" style={{ width: '18rem' , marginTop:'-1000px'}} >
                <Card.Body>
                 <Card.Title>Total : <strong>Rs{parseFloat(total).toFixed(2)}</strong></Card.Title> 
                 <Card.Title>Total Tax : <b>Rs{parseFloat(total * 0.05).toFixed(2)}</b></Card.Title>
                </Card.Body>
                <div>
                  <Button
                    variant="danger"
                                       onClick={() => {
                      dispatch(emptyCart());
                    }}
                  >
                    Empty Cart
                  </Button>
                </div>
              </Card>
            ) : (
              <div>
                <div>There is No Item in the Cart...</div>
              </div>
            )}
          </div>
        </div>
      </div>
    </div>
  );
};

export default Cart;
