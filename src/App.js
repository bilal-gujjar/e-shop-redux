import { Routes, Route, Link } from "react-router-dom";
import Container from "react-bootstrap/Container";
import Nav from "react-bootstrap/Nav";
import Navbar from "react-bootstrap/Navbar";
import "./App.css";
import Header from "./components/Header";
import Cart from "./components/Cart";


function App() {
  return (
    <div>
      <Navbar bg="dark" variant="dark">
        <Container>
          <Navbar.Brand> <Link to = "/" className="text-white  text-decoration-none">E-commerce Store with React Redux</Link></Navbar.Brand>
          <Nav className="me-auto">
           <Nav.Link> <Link to = "/" className="text-white  text-decoration-none" >Home</Link></Nav.Link>
            <Nav.Link><Link to="/cart" className="text-white  text-decoration-none" >Cart</Link></Nav.Link>
          </Nav>
        </Container>
      </Navbar>
      <br />
      <Routes>
        <Route path="/" element={<Header />} />
        <Route path="/cart" element={<Cart />} />
      </Routes>
    </div>
  );
}

export default App;

