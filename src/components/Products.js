import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import Product from "./Product";

import { fetchProducts } from "../shop/products/actionCreators";

const Products = () => {
  const products = useSelector((state) => state.products);
  const dispatch = useDispatch();
  useEffect(() => {
    fetch("https://fakestoreapi.com/products")
      .then((res) => res.json())
      .then((products) => dispatch(fetchProducts(products)));
  }, [dispatch]);
  return (
    <div id="custom-cards" className="container">
      <h2>Product Page</h2>

      <div className="row">
        {products.length > 0 &&
          products.map((product) => (
            <div className="col-sm-4" key={product.id}>
              <Product product={product} />
            </div>
          ))}
      </div>
    </div>
  );
};

export default Products;
